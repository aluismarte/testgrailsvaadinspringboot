package com.alsnightsoft.examples.app.utils

import groovy.transform.CompileStatic
import org.springframework.context.ConfigurableApplicationContext

/**
 *  Created by aluis on 10/19/15.
 */
@CompileStatic
@Singleton
class Constants {

    public ConfigurableApplicationContext applicationContext

    public void autoWiredClass(Object objectToWired) {
        applicationContext.getAutowireCapableBeanFactory().autowireBean(objectToWired)
    }
}

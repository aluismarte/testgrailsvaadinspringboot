package com.alsnightsoft.examples.app.domains

import grails.persistence.Entity

/**
 *  Created by aluis on 10/19/15.
 */
@Entity
class Student {

    String name
    String lastname

    static constraints = {
        lastname nullable: true
    }

    @Override
    String toString() {
        return name
    }
}

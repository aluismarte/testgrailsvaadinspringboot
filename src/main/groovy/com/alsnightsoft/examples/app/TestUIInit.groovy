package com.alsnightsoft.examples.app

import com.alsnightsoft.examples.app.utils.Constants
import com.vaadin.spring.server.SpringVaadinServlet
import com.vaadin.ui.UI
import groovy.transform.CompileStatic
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.embedded.ServletRegistrationBean
import org.springframework.boot.context.web.SpringBootServletInitializer
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Lazy
import org.springframework.context.annotation.Scope

/**
 *  Created by aluis on 10/19/15.
 */
@Lazy
@SpringBootApplication
@CompileStatic
class TestUIInit extends SpringBootServletInitializer implements Serializable {

    @Bean
    @Scope("prototype")
    public UI ui() {
        return new TestUI()
    }

    @Bean
    public ServletRegistrationBean servletRegistrationBean() {
        return new ServletRegistrationBean(new SpringVaadinServlet(), "/*", "/VAADIN/*")
    }

    public static void main(String[] args) throws Exception {
        Constants.instance.applicationContext = SpringApplication.run(TestUIInit.class, args)
        Constants.instance.autoWiredClass(this)
    }
}

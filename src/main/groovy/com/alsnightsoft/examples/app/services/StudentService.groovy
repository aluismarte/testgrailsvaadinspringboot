package com.alsnightsoft.examples.app.services

import com.alsnightsoft.examples.app.domains.Student
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by aluis on 10/19/15.
 */
@Service("studentService")
@Transactional(rollbackFor = Exception.class)
class StudentService {

    public Student saveStudent(Student student) {
        if (student != null && !student.hasErrors()) {
            def studentExist = student.get(student.id)
            if (studentExist) {
                return student.merge(flush: true, failOnError: true)
            } else {
                return student.save(flush: true, failOnError: true)
            }
        }
        return null
    }

    public List<Student> listStudents(int startIndex, int numberOfIds) {
        return Student.withCriteria {
            firstResult(startIndex)
            maxResults(numberOfIds)
        }.findAll()
    }

    public int countStudents() {
        return Student.count()
    }

}

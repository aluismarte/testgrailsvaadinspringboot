package com.alsnightsoft.examples.app

import com.alsnightsoft.examples.app.domains.Student
import com.alsnightsoft.examples.app.models.StudentContainer
import com.alsnightsoft.examples.app.services.StudentService
import com.alsnightsoft.examples.app.utils.Constants
import com.vaadin.annotations.Theme
import com.vaadin.event.SelectionEvent
import com.vaadin.server.VaadinRequest
import com.vaadin.spring.annotation.SpringUI
import com.vaadin.ui.Grid
import com.vaadin.ui.UI
import com.vaadin.ui.VerticalLayout
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by aluis on 10/19/15.
 */
@Theme("valo")
@SpringUI
class TestUI extends UI implements Serializable {

    private VerticalLayout mainLayout

    private Grid grid
    private StudentContainer studentContainer

    @Autowired
    StudentService studentService

    public TestUI() {
        Constants.instance.autoWiredClass(this)
    }

    @Override
    protected void init(VaadinRequest request) {
        setSizeFull()

        mainLayout = new VerticalLayout()
        mainLayout.setSizeFull()
        mainLayout.setMargin(true)

        createStudents()

        studentContainer = new StudentContainer(new LazyQuery<Student>() {
            @Override
            int getSize() {
                return studentService.countStudents()
            }

            @Override
            List<Student> getItemsIds(int startIndex, int numberOfIds) {
                return studentService.listStudents(startIndex, numberOfIds)
            }

            @Override
            int getFilteredSize() {
                return 0
            }

            @Override
            List<Student> getFilteredItemsIds(int startIndex, int numberOfIds) {
                return new ArrayList<Student>()
            }
        })

        grid = new Grid()
        grid.setSelectionMode(Grid.SelectionMode.SINGLE)
        grid.setImmediate(true)
        grid.setSizeFull()
        grid.setFooterVisible(true)
        grid.setContainerDataSource(studentContainer)
        grid.setColumnOrder("id", "name", "lastname")
        grid.removeColumn(BaseQueryFactory.OBJ)
        grid.addSelectionListener(new SelectionEvent.SelectionListener() {
            @Override
            void select(SelectionEvent event) {
                println "Student ID: " + ((Student) grid.getSelectedRow()).getId()
                println "Student Name: " + ((Student) grid.getSelectedRow()).getName()
            }
        })

        mainLayout.addComponent(grid)

        setContent(mainLayout)
    }

    private void createStudents() {
        studentService.saveStudent(new Student(name: "Aluis", lastname: "Marte"))
        studentService.saveStudent(new Student(name: "Ronald", lastname: "Marmol"))
        studentService.saveStudent(new Student(name: "Victor", lastname: "Martinez"))
    }
}

package com.alsnightsoft.examples.app.models

import com.alsnightsoft.examples.app.BaseQuery
import com.alsnightsoft.examples.app.BaseQueryFactory
import com.alsnightsoft.examples.app.domains.Student

/**
 *  Created by aluis on 10/19/15.
 */
class StudentQuery extends BaseQuery<Student> {

    public StudentQuery(BaseQueryFactory baseQueryFactory) {
        super(baseQueryFactory)
    }
}

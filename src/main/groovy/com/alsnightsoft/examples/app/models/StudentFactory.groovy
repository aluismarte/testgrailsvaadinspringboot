package com.alsnightsoft.examples.app.models

import com.alsnightsoft.examples.app.BaseQueryFactory
import com.alsnightsoft.examples.app.LazyQuery
import com.alsnightsoft.examples.app.domains.Student
import com.vaadin.data.Item
import org.vaadin.addons.lazyquerycontainer.Query
import org.vaadin.addons.lazyquerycontainer.QueryDefinition

/**
 *  Created by aluis on 10/19/15.
 */
class StudentFactory extends BaseQueryFactory<Student> {

    public StudentFactory(LazyQuery<Student> lazyQuery) {
        super(lazyQuery)
    }

    @Override
    Item constructItem() {
        return constructItem(new Student())
    }

    @Override
    Object createProperty(Object propertyID, Student dataObject) {
        switch (propertyID.toString()) {
            case OBJ:
                return dataObject
            case "id":
                return dataObject.getId()
            case "name":
                return dataObject.getName()
            case "lastname":
                return dataObject.getLastname()
        }
        return getDefaultProperty(propertyID)
    }

    @Override
    Query constructQuery(QueryDefinition queryDefinition) {
        this.queryDefinition = queryDefinition
        return new StudentQuery(this)
    }
}

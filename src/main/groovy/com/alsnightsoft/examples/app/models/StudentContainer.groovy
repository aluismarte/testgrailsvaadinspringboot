package com.alsnightsoft.examples.app.models

import com.alsnightsoft.examples.app.BaseContainer
import com.alsnightsoft.examples.app.LazyQuery
import com.alsnightsoft.examples.app.domains.Student

/**
 *  Created by aluis on 10/19/15.
 */
class StudentContainer extends BaseContainer {

    public StudentContainer(LazyQuery<Student> lazyQuery) {
        super(new StudentFactory(lazyQuery))

        addColumns()
    }

    @Override
    public void addColumns() {
        addContainerProperty("id", Long.class, 0l, false, false)
        addContainerProperty("name", String.class, "", false, false)
        addContainerProperty("lastname", String.class, "", false, false)
    }
}

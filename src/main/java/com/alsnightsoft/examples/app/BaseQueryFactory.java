package com.alsnightsoft.examples.app;

import com.vaadin.data.Item;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;
import org.vaadin.addons.lazyquerycontainer.QueryDefinition;
import org.vaadin.addons.lazyquerycontainer.QueryFactory;

/**
 *  Created by aluis on 10/8/15.
 */
public abstract class BaseQueryFactory<BEANTYPE> implements QueryFactory {

    public static final String OBJ = "OBJ";
    public static final int BATCH_GRID_SIZE = 20;
    protected QueryDefinition queryDefinition;
    protected LazyQuery<BEANTYPE> lazyQuery;

    protected BaseQueryFactory(LazyQuery<BEANTYPE> lazyQuery) {
        this.lazyQuery = lazyQuery;
    }

    public abstract Item constructItem();

    @SuppressWarnings("unchecked")
    public Item constructItem(BEANTYPE beantype) {
        PropertysetItem item = new PropertysetItem();
        for (Object propertyId : this.queryDefinition.getPropertyIds()) {
            Object value = createProperty(propertyId, beantype);
            item.addItemProperty(propertyId, new ObjectProperty(value,
                    queryDefinition.getPropertyType(propertyId),
                    queryDefinition.isPropertyReadOnly(propertyId)
            ));
        }
        return item;
    }

    public abstract Object createProperty(Object propertyID, BEANTYPE dataObject);

    protected Object getDefaultProperty(Object propertyID) {
        return queryDefinition.getPropertyDefaultValue(propertyID);
    }

    public QueryDefinition getQueryDefinition() {
        return queryDefinition;
    }

    public void setQueryDefinition(QueryDefinition queryDefinition) {
        this.queryDefinition = queryDefinition;
    }

    public LazyQuery<BEANTYPE> getLazyQuery() {
        return lazyQuery;
    }

    public void setLazyQuery(LazyQuery<BEANTYPE> lazyQuery) {
        this.lazyQuery = lazyQuery;
    }
}
